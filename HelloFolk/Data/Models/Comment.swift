//
//  Comment.swift
//  HelloFolk
//
//  Created by Yahya Tabba on 3/1/21.
//

import Foundation

struct Comment:  Codable {
    var postId : Int
    var id : Int
    var name : String
    var email : String
    var body : String        
}

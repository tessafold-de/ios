//
//  Post.swift
//  HelloFolk
//
//  Created by Yahya Tabba on 3/1/21.
//

import Foundation

struct Post:  Codable {
    var userId : Int
    var id : Int
    var title : String
    var body : String
    
    var user : User?        
}

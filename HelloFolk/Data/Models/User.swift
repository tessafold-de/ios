//
//  User.swift
//  HelloFolk
//
//  Created by Yahya Tabba on 3/1/21.
//

import Foundation

struct User:  Codable {
    var id : Int
    var name : String
    var username : String
        
}

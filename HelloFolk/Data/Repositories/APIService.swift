//
//  APIService.swift
//  HelloFolk
//
//  Created by Yahya Tabba on 3/1/21.
//

import Foundation
import Alamofire

class APIService :  NSObject {
    
    static let shared = APIService()
    
    let postsURL = "http://jsonplaceholder.typicode.com/posts"
    let usersURL = "http://jsonplaceholder.typicode.com/users"
    let commentsURL = "http://jsonplaceholder.typicode.com/comments"
    
    func getPosts(completion : @escaping ([Post]) -> ()){
        
        getUsers { (users) in
            // Get from local file
            if FileUtility<[Post]>.isFileExists(filename: .posts),
               var posts = FileUtility<[Post]>.loadJSON(withFilename: .posts) as? [Post]{
                print("LOCAL")
                for i in 0..<posts.count{posts[i].user = users.first(where: {$0.id == posts[i].userId})}
                completion(posts)
            }
            //Get from the live server
            else{
                AF.request(URL(string: self.postsURL)!).responseData { (response) in
                    switch response.result{
                    
                    case .success(let value):
                        let jsonDecoder = JSONDecoder()
                        var posts = try! jsonDecoder.decode([Post].self, from: value)
                        if FileUtility<[Post]>.save(jsonObject: posts, toFilename: .posts){
                            print("SAVED LOCAL")
                        }
                        for i in 0..<posts.count{posts[i].user = users.first(where: {$0.id == posts[i].userId})}
                        completion(posts)
                    case .failure(let error):
                        print(error.localizedDescription)
                    }
                }
            }
        }
    }
    
    func getComments(completion : @escaping ([Comment]) -> ()){
        // Get from local file
        if FileUtility<[Comment]>.isFileExists(filename: .posts),
           let comments = FileUtility<[Comment]>.loadJSON(withFilename: .comments) as? [Comment]{
            print("LOCAL2")
            completion(comments)
        }
        //Get from the live server
        else{
            AF.request(URL(string: self.commentsURL)!).responseData { (response) in
                switch response.result{
                
                case .success(let value):
                    let jsonDecoder = JSONDecoder()
                    let comments = try! jsonDecoder.decode([Comment].self, from: value)
                    if FileUtility<[Comment]>.save(jsonObject: comments, toFilename: .comments){
                        print("SAVED LOCAL2")
                    }
                    completion(comments)
                case .failure(let error):
                    print(error.localizedDescription)
                }
            }
        }
    }
    
    func getUsers(completion : @escaping ([User]) -> ()){
        // Get from local file
        if FileUtility<[User]>.isFileExists(filename: .users),
           let users = FileUtility<[User]>.loadJSON(withFilename: .users) as? [User]{
            print("LOCAL3")
            completion(users)
        }
        //Get from the live server
        else{
            AF.request(URL(string: self.usersURL)!).responseData { (response) in
                switch response.result{
                
                case .success(let value):
                    let jsonDecoder = JSONDecoder()
                    let users = try! jsonDecoder.decode([User].self, from: value)
                    if FileUtility<[User]>.save(jsonObject: users, toFilename: .users){
                        print("SAVED LOCAL3")
                    }
                    completion(users)
                case .failure(let error):
                    print(error.localizedDescription)
                }
            }
        }
    }
    
    
    
    
}

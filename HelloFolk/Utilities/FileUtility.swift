//
//  FileUtility.swift
//  HelloFolk
//
//  Created by Yahya Tabba on 3/1/21.
//

import Foundation

enum FileName : String {
    case posts
    case users
    case comments
}

enum FileUtility<Value: Codable>{
    
    static func loadJSON(withFilename filename: FileName) -> Any? {
        do{
            let fileManager = FileManager.default
            let urls = fileManager.urls(for: .documentDirectory, in: .userDomainMask)
            if let url = urls.first {
                var fileURL = url.appendingPathComponent(filename.rawValue)
                fileURL = fileURL.appendingPathExtension("json")
                let data = try Data(contentsOf: fileURL)
                let jsonObject = try JSONDecoder().decode(Value.self, from: data)
                return jsonObject
            }
        }catch let err{
            print(err.localizedDescription)
        }
        return nil
    }
    
    static func save(jsonObject: Value, toFilename filename: FileName) -> Bool{
        do{
            let fileManager = FileManager.default
            let urls = fileManager.urls(for: .documentDirectory, in: .userDomainMask)
            if let url = urls.first {
                var fileURL = url.appendingPathComponent(filename.rawValue)
                fileURL = fileURL.appendingPathExtension("json")
                let data = try JSONEncoder().encode(jsonObject)
                
                //Checking file is available or now
                guard fileManager.fileExists(atPath: fileURL.path) else {
                    /// Create new file here
                    return fileManager.createFile(atPath: fileURL.path, contents: data, attributes: nil)
                }
                try data.write(to: fileURL, options: [.atomicWrite])
                return true
            }
            return false
        }catch let err{
            print(err.localizedDescription)
            return false
        }
    }
    
    static func isFileExists(filename: FileName) -> Bool{
        let fileManager = FileManager.default
        let urls = fileManager.urls(for: .documentDirectory, in: .userDomainMask)
        if let url = urls.first {
            var fileURL = url.appendingPathComponent(filename.rawValue)
            fileURL = fileURL.appendingPathExtension("json")
            return fileManager.fileExists(atPath: fileURL.path)
        }
        return false
    }
    
}

//
//  CommentCell.swift
//  HelloFolk
//
//  Created by Yahya Tabba on 3/1/21.
//

import UIKit

class CommentCell: UITableViewCell {

    static let identifier  = "CommentCell"
    
    @IBOutlet weak var nameLbl : UILabel!
    @IBOutlet weak var bodyLbl : UILabel!
    
    
    var comment : CommentViewModel!{
        didSet{
            self.nameLbl.text = self.comment.name
            self.bodyLbl.text = self.comment.body
        }
    }

    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        
        self.accessibilityIdentifier = CommentCell.identifier
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}

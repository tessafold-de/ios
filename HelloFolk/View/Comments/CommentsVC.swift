//
//  CommentsVC.swift
//  HelloFolk
//
//  Created by Yahya Tabba on 3/1/21.
//

import UIKit

class CommentsVC: UIViewController {

    @IBOutlet weak var tableView : UITableView!

    var post : PostViewModel!
    var viewModel : CommentsViewModel!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        setupUI()
    }
    
    
    func setupUI(){
        
        //Setup Navigation controller style
        self.setupNavigationStyle()

        self.viewModel = CommentsViewModel(post: self.post)
        
        //TableView Delegate - DataSource
        self.tableView.accessibilityIdentifier = "Comments Table"
        self.tableView.delegate = self.viewModel
        self.tableView.dataSource = self.viewModel
        self.tableView.register(UINib(nibName: PostCell.identifier, bundle: nil), forCellReuseIdentifier: PostCell.identifier)
        self.tableView.register(UINib(nibName: CommentCell.identifier, bundle: nil), forCellReuseIdentifier: CommentCell.identifier)
        self.tableView.tableFooterView = UIView()
                
        //GET DATA
        self.viewModel.bindCommentsViewModelToController = {
            DispatchQueue.main.async {
                self.reloadData()
            }
        }

    }
    
    /// setup navigation bar style
    func setupNavigationStyle(){
        //NAVIGATION BAR STYLE
        self.navigationController?.navigationBar.barTintColor = .white
        self.navigationController?.navigationBar.isTranslucent = false
        self.navigationController?.navigationBar.shadowImage = UIImage()
        self.navigationController?.navigationBar.tintColor = .black
        self.navigationController?.navigationBar.setBackgroundImage(UIImage(), for: .default)
        self.navigationItem.leftBarButtonItem = UIBarButtonItem.init(image: #imageLiteral(resourceName: "leftTop"), style: .plain, target: self, action: #selector(self.close))
    }
    
    /// reload data in the view after fetched
    func reloadData(){
        tableView.reloadData()
    }
    
    /// Close the view and back to posts view
    @objc func close(){
        self.dismiss(animated: false, completion: nil)
    }
    

    
}

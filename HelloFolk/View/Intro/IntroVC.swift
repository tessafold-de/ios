//
//  IntroVC.swift
//  HelloFolk
//
//  Created by Yahya Tabba on 2/28/21.
//

import UIKit
import Lottie

class IntroVC: UIViewController {

    @IBOutlet weak var animationView : AnimationView!
    @IBOutlet weak var leftBtn : UIButton!
    var index : Int = 1
    
    override func viewDidLoad() {
        super.viewDidLoad()

        self.leftBtn.accessibilityIdentifier = "test1"
        self.setupViews()
    }
    
    func setupViews(){
        self.leftBtn.isHidden = (index == 1)
        animationView.animation = Animation.named(index == 1 ? "charachter1" : "charachter2")
        animationView.animationSpeed = 5
        animationView.loopMode = .autoReverse
        animationView.play()
    }
    
    /// to move to the next view
    @IBAction func rightAction(){
        if index == 2{
            //GO TO HOME
            let vc = self.storyboard?.instantiateViewController(withIdentifier: "FeedVC") as! FeedVC
            let nv = UINavigationController.init(rootViewController: vc)
            nv.modalPresentationStyle = .fullScreen
            self.present(nv, animated: false, completion: nil)
        }else{
            self.index = 2
            setupViews()
        }
    }
    
    /// to move to the previous view
    @IBAction func leftAction(){
        self.index = 1
        self.setupViews()
    }
    
}

//
//  FirstVC.swift
//  HelloFolk
//
//  Created by Yahya Tabba on 2/28/21.
//

import UIKit

class FirstVC: UIViewController {

    @IBOutlet weak var img : UIImageView!
    
    let pagesStyle = [(UIColor(red: 251/255, green: 173/255, blue: 24/255, alpha: 1),#imageLiteral(resourceName: "logo_white")),
                  (UIColor(red: 95/255, green: 64/255, blue: 144/255, alpha: 1),#imageLiteral(resourceName: "logo_white")),
                  (UIColor(red: 2/255, green: 213/255, blue: 180/255, alpha: 1),#imageLiteral(resourceName: "logo_white")),
                  (UIColor.white,#imageLiteral(resourceName: "logo_colored"))]
    var timer = Timer()
    var animatorIndex = 0
    var timerSpeed : TimeInterval = 0.5
    
    override func viewDidLoad() {
        super.viewDidLoad()

        self.view.backgroundColor = self.pagesStyle[0].0
        timer = Timer.scheduledTimer(timeInterval: timerSpeed, target: self, selector: #selector(timerAction), userInfo: nil, repeats: true)
    }
    
    /// Action triggers every 0.5 secounds to change the color of view  every opening the view
    @objc func timerAction() {
        if animatorIndex < self.pagesStyle.count{
            UIView.animate(withDuration: timerSpeed) {
                self.img.image = self.pagesStyle[self.animatorIndex].1
                self.view.backgroundColor = self.pagesStyle[self.animatorIndex].0
            }
            self.animatorIndex += 1
        }else{
            self.timer.invalidate()
            self.openIntro()
        }
    }
    
    /// Move to intro view after finish the animations
    func openIntro(){
        let vc = self.storyboard?.instantiateViewController(withIdentifier: "IntroVC") as! IntroVC
        vc.modalPresentationStyle = .fullScreen
        self.present(vc, animated: false, completion: nil)
    }


    


}

//
//  FeedVC.swift
//  HelloFolk
//
//  Created by Yahya Tabba on 3/1/21.
//

import UIKit

class FeedVC: UIViewController {

    @IBOutlet weak var tableView : UITableView!

    var viewModel = FeedViewModel()
    
    override func viewDidLoad() {
        super.viewDidLoad()

        setupUI()
    }
    
    func setupUI(){

        //Setup Navigation controller style
        self.setupNavigationStyle()
        
        //TableView Delegate - DataSource
        self.tableView.accessibilityIdentifier = "Feed Table"
        self.tableView.delegate = self.viewModel
        self.tableView.dataSource = self.viewModel
        self.tableView.register(UINib(nibName: PostCell.identifier, bundle: nil), forCellReuseIdentifier: PostCell.identifier)
        self.tableView.tableFooterView = UIView()
        
        //GET DATA
        self.viewModel.bindPostsViewModelToController = {
            DispatchQueue.main.async {
                self.reloadData()
            }
        }

        //When Open Comments
        self.viewModel.openComments = { post in
            self.openComments(post)
        }
        
    }

    
    func setupNavigationStyle(){
        //NAVIGATION BAR STYLE
        let navColor = UIColor(red: 85/255, green: 0, blue: 150/255, alpha: 1)
        self.navigationController?.navigationBar.barTintColor = navColor
        self.navigationController?.navigationBar.isTranslucent = false
        self.navigationController?.navigationBar.shadowImage = UIImage()
        self.navigationController?.navigationBar.tintColor = .white
        self.navigationController?.navigationBar.setBackgroundImage(UIImage(), for: .default)
        self.addRoundedCorner(OnNavigationBar: self.navigationController!.navigationBar, cornerRadius: 30,color : navColor)

        let vv = UIView(frame: CGRect.init(x: 0, y: 12, width: 300, height: 30))
        let imageView = UIImageView(frame: vv.frame)
        imageView.image = #imageLiteral(resourceName: "logo_white")
        imageView.contentMode = .scaleAspectFit
        vv.addSubview(imageView)
        self.navigationItem.titleView = vv
    }
    
    /// reload data in the view after fetched
    func reloadData(){
        tableView.reloadData()
        
        self.view.accessibilityIdentifier = "trt"
    }
    
    /// to move to the comments view
    /// - Parameter post: send the post selected to the comments view
    func openComments(_ post : PostViewModel){
        let vc = self.storyboard?.instantiateViewController(withIdentifier: "CommentsVC") as! CommentsVC
        vc.post = post
        let nv = UINavigationController(rootViewController: vc)
        nv.modalPresentationStyle = .fullScreen
        self.present(nv, animated: false, completion: nil)
    }
    
    /// Add rounded to the navigation bar
    /// - Parameters:
    ///   - navigationBar: the selected navigation bar
    ///   - cornerRadius: corner radius value
    ///   - color: selected color 
    func addRoundedCorner(OnNavigationBar navigationBar: UINavigationBar, cornerRadius: CGFloat, color : UIColor){
        navigationBar.isTranslucent = false
        navigationBar.setBackgroundImage(UIImage(), for: .default)
        navigationBar.backgroundColor = color
        
        let customView = UIView(frame: CGRect(x: 0, y: navigationBar.bounds.maxY, width: navigationBar.bounds.width, height: cornerRadius))
        customView.backgroundColor = .clear
        navigationBar.insertSubview(customView, at: 1)
        
        let shapeLayer = CAShapeLayer()
        shapeLayer.path = UIBezierPath(roundedRect: customView.bounds, byRoundingCorners: [.bottomLeft,.bottomRight], cornerRadii: CGSize(width: cornerRadius, height: cornerRadius)).cgPath
        
        shapeLayer.shadowColor = UIColor.lightGray.cgColor
        shapeLayer.shadowOffset = CGSize(width: 0, height: 4.0)
        shapeLayer.shadowOpacity = 0.8
        shapeLayer.shadowRadius = 2
        shapeLayer.fillColor = color.cgColor
        customView.layer.insertSublayer(shapeLayer, at: 0)
    }

    
}

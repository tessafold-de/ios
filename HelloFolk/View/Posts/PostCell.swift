//
//  PostCell.swift
//  HelloFolk
//
//  Created by Yahya Tabba on 3/1/21.
//

import UIKit

class PostCell: UITableViewCell {

    static let identifier  = "PostCell"
    
    @IBOutlet weak var userNameLbl : UILabel!
    @IBOutlet weak var titleLbl : UILabel!
    @IBOutlet weak var bodyLbl : UILabel!
    
    
    var post : PostViewModel!{
        didSet{
            self.userNameLbl.text = self.post.userName
            self.titleLbl.text = self.post.title
            self.bodyLbl.text = self.post.body
        }
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        
        self.accessibilityIdentifier = PostCell.identifier
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}

//
//  CommentViewModel.swift
//  HelloFolk
//
//  Created by Yahya Tabba on 3/1/21.
//

import Foundation

class CommentViewModel{
    
    var comment : Comment
    
    init(_ comment : Comment) {
        self.comment = comment
    }
    
    var name : String{
        return self.comment.name
    }
    
    var body : String{
        return self.comment.body
    }
    


}

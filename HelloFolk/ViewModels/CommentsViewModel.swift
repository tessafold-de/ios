//
//  CommentsViewModel.swift
//  HelloFolk
//
//  Created by Yahya Tabba on 3/1/21.
//

import Foundation
import UIKit

class CommentsViewModel : NSObject{
    
    var post : PostViewModel
    var comments : [CommentViewModel] = []

    var bindCommentsViewModelToController : (() -> ()) = {}
    
    init(post : PostViewModel) {
        self.post = post
        super.init()
        fetchData()
    }

    @objc func fetchData(){
        APIService.shared.getComments { (res) in
            self.comments = res.filter({$0.postId == self.post.postId}).map({CommentViewModel($0)})
            self.bindCommentsViewModelToController()
        }
    }
    
}



//MARK: - TableView DataSource
extension CommentsViewModel : UITableViewDataSource{
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return comments.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: CommentCell.identifier, for: indexPath) as! CommentCell
        cell.selectionStyle = .none
        cell.comment = self.comments[indexPath.row]
        return cell
    }
}

//MARK: - TableView Delegate
extension CommentsViewModel : UITableViewDelegate{
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        //
    }
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableView.automaticDimension
    }
    
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        let headerView = UIView(frame: CGRect.init(x: 0, y: 0, width: tableView.bounds.width, height: 216))
        let cell = tableView.dequeueReusableCell(withIdentifier: PostCell.identifier) as! PostCell
        cell.post = self.post
        cell.frame.size.height = 200
        cell.frame.size.width = tableView.bounds.width
        headerView.backgroundColor = UIColor(red: 0.949, green: 0.949, blue: 0.949, alpha: 1)
        headerView.addSubview(cell)
        return headerView
    }
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return 216
    }
}

//
//  FeedViewModel.swift
//  HelloFolk
//
//  Created by Yahya Tabba on 3/1/21.
//

import Foundation
import UIKit

class FeedViewModel : NSObject{
    
    var posts : [PostViewModel] = []

    var bindPostsViewModelToController : (() -> ()) = {}
    var openComments : ((PostViewModel) -> ())!
    
    override init() {
        super.init()
        fetchData()
    }

    @objc func fetchData(){
        APIService.shared.getPosts { (res) in
            self.posts = res.map({PostViewModel($0)})
            self.bindPostsViewModelToController()
        }
    }
    
    
}

//MARK: - TableView DataSource
extension FeedViewModel : UITableViewDataSource{
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return posts.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: PostCell.identifier, for: indexPath) as! PostCell
        cell.selectionStyle = .none
        cell.post = self.posts[indexPath.row]
        return cell
    }
}

//MARK: - TableView Delegate
extension FeedViewModel : UITableViewDelegate{
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        self.openComments(self.posts[indexPath.row])
    }
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableView.automaticDimension
    }
}





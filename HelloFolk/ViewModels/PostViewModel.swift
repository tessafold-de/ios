//
//  PostViewModel.swift
//  HelloFolk
//
//  Created by Yahya Tabba on 3/1/21.
//

import Foundation


class PostViewModel{
    
    var post : Post
    
    init(_ post : Post) {
        self.post = post
    }
    
    var postId : Int{
        self.post.id
    }
    
    var title : String{
        return self.post.title
    }
    
    var body : String{
        return self.post.body
    }
    
    var userName : String!{
        return self.post.user?.name
    }
    


}

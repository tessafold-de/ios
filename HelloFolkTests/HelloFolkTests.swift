//
//  HelloFolkTests.swift
//  HelloFolkTests
//
//  Created by Yahya Tabba on 3/1/21.
//

import XCTest
@testable import HelloFolk

class HelloFolkTests: XCTestCase {

    var sut: APIService!

    override func setUp() {
      super.setUp()
        sut = APIService.shared
    }
    
    override func tearDown() {
      sut = nil
      super.tearDown()
    }
    
    func testIfPostsExist() {
        sut.getPosts { (posts) in
            XCTAssert(!posts.isEmpty)
        }
    }
    
    func testIfUsersExist() {
        sut.getUsers { (users) in
            XCTAssert(!users.isEmpty)
        }
    }
    
    func testIfCommentsExist() {
        sut.getComments { (comments) in
            XCTAssert(!comments.isEmpty)
        }
    }

    func testCreateUserModel() {
        let post = Post(userId: 1, id : 1, title : "title test", body : "body test")
        let viewModel = PostViewModel(post)
        XCTAssertEqual(viewModel.title, post.title)
    }



}

//
//  HelloFolkUITests.swift
//  HelloFolkUITests
//
//  Created by Yahya Tabba on 3/2/21.
//

import XCTest

class HelloFolkUITests: XCTestCase {
    var app: XCUIApplication!
    
    override func setUp() {
        continueAfterFailure = false
        
        app = XCUIApplication()
        app.launch()
    }
    
    func testCheckIfTableViewCellsExists() {
        //Click on the right button twice in Intro VC
        let rightButton = app.buttons["right"]
        rightButton.tap()
        rightButton.tap()
        
        //Check if feed table view exist
        let tableView1 = app.tables["Feed Table"]
        XCTAssert(tableView1.exists)
        
        //Check if feed table view has PostCell
        let postCell = tableView1.cells["PostCell"]
        XCTAssert(postCell.exists)
        
        //Click to open comments screen from a post
        tableView1.cells.firstMatch.tap()
        
        //Check if comments table view exist
        let tableView2 = app.tables["Comments Table"]
        XCTAssert(tableView2.exists)
        
        //Check if comments table view has CommentCell
        let commentCell = tableView2.cells["CommentCell"]
        XCTAssert(commentCell.exists)
    }
    
}
